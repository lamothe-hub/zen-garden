<h1>CSS Zen Garden</h1>
<p>By Alexandra Bagarra, Jeffrey Lamothe, Barry Otieno, Cameron Kasten, and Rachel Klesius</p>

<h2>How to use this program</h2>
<p>Simply open index.html using your favorite browser.  This program was tested using Chrome.  </p>

<h2>Artistic Inspiration</h2>
<p>When we were children, we grew up in the age of raw CSS.  
We wanted to restore the beauty we found in websites of your youth with this webpage. Everything is so complex with frameworks and bootstrap and websites trying to be clean, concise, even usable.
We remember simpler times, better times, with cyan and magenta; with Internet Explorer (RIP) and dancing bananas.  </p>